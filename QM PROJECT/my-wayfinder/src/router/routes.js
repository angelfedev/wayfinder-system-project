const routes = [
  {
    path: "/",
    name: "wayfinder-system",
    children: [
      {
        path: "home-start",
        name: "home-start",
        component: () => import("src/pages/HomeStart.vue"),
      },
      {
        path: "current-floor",
        name: "current-floor",
        component: () => import("src/pages/CurrentFloor.vue"),
      },
      {
        path: "target-location1",
        name: "target-location1",
        component: () => import("src/pages/TargetLocation1.vue"),
      },
      {
        path: "target-location2",
        name: "target-location2",
        component: () => import("src/pages/TargetLocation2.vue"),
      },
      {
        path: "target-location3",
        name: "target-location3",
        component: () => import("src/pages/TargetLocation3.vue"),
      },
      {
        path: "location-map1",
        name: "location-map1",
        component: () => import("src/pages/LocationMap1.vue"),
      },
      {
        path: "location-map2",
        name: "location-map2",
        component: () => import("src/pages/LocationMap2.vue"),
      },
      {
        path: "location-map3",
        name: "location-map3",
        component: () => import("src/pages/LocationMap3.vue"),
      },
    ]

  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
